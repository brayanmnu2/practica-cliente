package com.co.pragma.crecimiento.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDto {

	private Long id;
	@NotEmpty(message="Los nombres no pueden ser vacíos")
	private String nombres;
	
	@NotEmpty(message="Los apellidos no pueden ser vacíos")
	private String apellidos;
	
	@NotNull(message = "Tipo de identificacion no puede ser nulo")
	private TipoIdentificacionDto tipoIdentificacion;
	
	@NotEmpty(message="El número de identificación no puede ser vacío")
	private String nroIdentificacion;
	
	@Positive(message="La edad no puede ser negativa")
	private int edad;
	private String ciudadNacimiento;
}
