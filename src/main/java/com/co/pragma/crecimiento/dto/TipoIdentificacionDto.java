package com.co.pragma.crecimiento.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoIdentificacionDto {

	private Long id;
	private String descripcion;
}
