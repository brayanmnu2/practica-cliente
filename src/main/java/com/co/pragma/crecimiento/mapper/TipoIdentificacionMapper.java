package com.co.pragma.crecimiento.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;

@Mapper(componentModel = "spring")
public interface TipoIdentificacionMapper {

	TipoIdentificacionDto tipoIdentificacionEntityToTipoIdentificacionDto(TipoIdentificacionEntity tipoEntity);

	TipoIdentificacionEntity tipoIdentificacionDtoToTipoIdentificacionEntity(
			TipoIdentificacionDto tipoIdentificacionDto);

	List<TipoIdentificacionDto> listTipoIdentificacionEntityToListTipoIdentificacionDto(List<TipoIdentificacionEntity> tipoEntity);

	List<TipoIdentificacionEntity> listTipoIdentificacionDtoToListTipoIdentificacionEntity(
			List<TipoIdentificacionDto> tipoIdentificacionDto);

}
