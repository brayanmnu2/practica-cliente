package com.co.pragma.crecimiento.mapper;

import java.util.List;
import org.mapstruct.Mapper;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.model.ClienteEntity;

@Mapper(componentModel = "spring")
public interface ClienteMapper {


	ClienteDto clientEntityToClienteDto (ClienteEntity clienteEntity);
	
	ClienteEntity clientDtoToClienteEntity (ClienteDto clienteDto);
	
	List<ClienteEntity> listClienteDtoToListClienteEntity (List<ClienteDto> listClienteDto);
	
	List<ClienteDto> listClienteEntityToListClienteDto (List<ClienteEntity> listClienteEntity);
}
