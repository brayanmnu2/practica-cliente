package com.co.pragma.crecimiento.util;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.model.ClienteEntity;
import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;

public class DatosTest {

	public static final TipoIdentificacionEntity TIPO_IDENTIFICACION_ENTITY_DNI = new TipoIdentificacionEntity(1L, "DNI");
	public static final TipoIdentificacionEntity TIPO_IDENTIFICACION_ENTITY_PASAPORTE = new TipoIdentificacionEntity(2L, "PASAPORTE");
	public static final TipoIdentificacionEntity TIPO_IDENTIFICACION_ENTITY_CC = new TipoIdentificacionEntity(3L, "CCI");

	public static final ClienteEntity CLIENTE_ENTITY_001 = new ClienteEntity(1L, "Brayan Michel", "Neyra Uriarte", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153630", 24, "Trujillo");
	
	public static final ClienteEntity CLIENTE_ENTITY_ACTUALIZAR_001 = new ClienteEntity(1L, "Brayitan Michel", "Neyra Uriarte", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153630", 24, "Trujillo");
	
	public static final ClienteEntity CLIENTE_ENTITY_002 = new ClienteEntity(2L, "Pedro", "Armas", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153631", 19, "Lima");
	public static final ClienteEntity CLIENTE_ENTITY_003 = new ClienteEntity(3L, "Pablo", "Nazario", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153632", 30, "Arequipa");
	
	public static final ClienteEntity CLIENTE_ENTITY_004 = new ClienteEntity(4L, "Juanito", "Crespo", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153633", 15, "Cusco");
	
	public static final ClienteEntity CLIENTE_ENTITY_SIN_ID_004 = new ClienteEntity(null, "Juanito", "Crespo", TIPO_IDENTIFICACION_ENTITY_DNI,
			"75153633", 15, "Cusco");
	
	public static final Long UNO_LONG = 1L;
	public static final Long DOS_LONG = 2L;
	public static final Long TRES_LONG = 3L;
	public static final Long CUATRO_LONG = 4L;
	
	public static final String DNI_001 = "75153630";
	public static final String DNI_002 = "75153631";
	public static final String DNI_003 = "75153632";
	public static final String DNI_004 = "75153633";
	
	public static final String DNI_DESC = "DNI";
	public static final String PASAPORTE_DESC = "PASAPORTE";
	public static final String CCI_DESC = "CCI";
	
	public static final int EDAD_19=19;
	public static final int EDAD_22=22;
	public static final int EDAD_27=27;
	
	
	public static final List<ClienteEntity> LIST_CLIENTE_19 = Arrays.asList(CLIENTE_ENTITY_001,CLIENTE_ENTITY_002,CLIENTE_ENTITY_003);
	public static final List<ClienteEntity> LIST_CLIENTE_22 = Arrays.asList(CLIENTE_ENTITY_001,CLIENTE_ENTITY_003);
	public static final List<ClienteEntity> LIST_CLIENTE_27 = Arrays.asList(CLIENTE_ENTITY_003);
	
	
	public static final Optional<ClienteEntity> CLIENTE_ENTITY_OPTIONAL_001 = Optional.of(CLIENTE_ENTITY_001);

	public static final Optional<TipoIdentificacionEntity> TIPO_IDENTIFICACION_ENTITY_DNI_OPTIONAL = Optional.of(TIPO_IDENTIFICACION_ENTITY_DNI);

	
	public static final TipoIdentificacionDto TIPO_IDENTIFICACION_DTO_DNI = new TipoIdentificacionDto(1L, "DNI");

	public static final ClienteDto CLIENTE_DTO_004 = new ClienteDto(null, "Juanito", "Crespo", TIPO_IDENTIFICACION_DTO_DNI,
			"75153633", 15, "Cusco");
	
	public static final ClienteDto CLIENTE_DTO_001 = new ClienteDto(1L, "Brayitan Michel", "Neyra Uriarte", TIPO_IDENTIFICACION_DTO_DNI,
			"75153630", 24, "Trujillo");
	
	public static final ClienteDto CLIENTE_DTO_001_SIN_VALID = new ClienteDto(1L, "Brayitan Michel", "", TIPO_IDENTIFICACION_DTO_DNI,
			"75153630", 24, "Trujillo");
	
	public static final ClienteDto CLIENTE_DTO_SIN_ID_001 = new ClienteDto(null, "Brayan Michel", "Neyra Uriarte", TIPO_IDENTIFICACION_DTO_DNI,
			"75153630", 24, "Trujillo");	
	
	public static final ClienteDto CLIENTE_DTO_SIN_ID_SIN_VALID_001 = new ClienteDto(null, "", "Neyra Uriarte", TIPO_IDENTIFICACION_DTO_DNI,
			"75153630", 24, "Trujillo");
	
	public static final Optional<TipoIdentificacionEntity> TIPO_ENTITY_OPTIONAL_EMPTY = Optional.empty();
	public static final Optional<ClienteEntity> CLIENTE_ENTITY_OPTIONAL_EMPTY = Optional.empty();
}
