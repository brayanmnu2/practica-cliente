package com.co.pragma.crecimiento.service;

import java.util.List;

import com.co.pragma.crecimiento.dto.ClienteDto;

public interface ClienteService {

	public List<ClienteDto> listarClientes();
	
	public ClienteDto registrarCliente(ClienteDto clienteDto);
	
	public ClienteDto actualizarCliente(ClienteDto clienteDto);
	
	public boolean eliminarClientePorId(Long idCliente);
	
	public ClienteDto buscarPorTipoNroIdentificacion(Long idTipo, String nroIdentificacion);
	
	public List<ClienteDto> buscarPorMayorIgualEdad(int edad);
	
	public ClienteDto verDetalleCliente(Long idCliente);
	
}
