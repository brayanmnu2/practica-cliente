package com.co.pragma.crecimiento.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.model.ClienteEntity;
import com.co.pragma.crecimiento.repository.ClienteRepository;
import com.co.pragma.crecimiento.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	private static final Logger logger = LogManager.getLogger(ClienteServiceImpl.class);

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClienteMapper clienteMapper;

	@Override
	public List<ClienteDto> listarClientes() {
		List<ClienteEntity> listClienteEntity = clienteRepository.findAll();
		return clienteMapper.listClienteEntityToListClienteDto(listClienteEntity);
	}

	@Override
	public ClienteDto registrarCliente(ClienteDto clienteDto) {
		ClienteDto clienteNroDoc = buscarPorTipoNroIdentificacion(clienteDto.getTipoIdentificacion().getId(),
				clienteDto.getNroIdentificacion());
		if (null != clienteNroDoc) {
			return null;	
		}
		ClienteEntity clienteEntity = clienteMapper.clientDtoToClienteEntity(clienteDto);
		try {
			clienteEntity = clienteRepository.saveAndFlush(clienteEntity);

		} catch (PersistenceException ex) {
			logger.error("Error en la persistencia: ", ex.getMessage());
		}

		return clienteMapper.clientEntityToClienteDto(clienteEntity);
	}

	@Override
	public ClienteDto actualizarCliente(ClienteDto clienteDto) {
		ClienteDto clienteBusqueda = verDetalleCliente(clienteDto.getId());
		if(null==clienteBusqueda) {
			return null;
		}
		ClienteEntity clienteEntity = clienteMapper.clientDtoToClienteEntity(clienteDto);
		clienteEntity = clienteRepository.saveAndFlush(clienteEntity);
		return clienteMapper.clientEntityToClienteDto(clienteEntity);
	}

	@Override
	public boolean eliminarClientePorId(Long idCliente) {
		ClienteDto clienteDto = verDetalleCliente(idCliente);
		if (null == clienteDto) {
			return false;
		}
		ClienteEntity clienteEntity = clienteMapper.clientDtoToClienteEntity(clienteDto);
		clienteRepository.delete(clienteEntity);
		return true;
	}

	@Override
	public ClienteDto buscarPorTipoNroIdentificacion(Long idTipo, String nroIdentificacion) {
		ClienteEntity clienteEntity = clienteRepository.buscarPorTipoNroIdentificacion(idTipo, nroIdentificacion);
		return clienteMapper.clientEntityToClienteDto(clienteEntity);
	}

	@Override
	public List<ClienteDto> buscarPorMayorIgualEdad(int edad) {
		List<ClienteEntity> clienteEntityList = clienteRepository.buscarPorMayorIgualEdad(edad);
		return clienteMapper.listClienteEntityToListClienteDto(clienteEntityList);
	}

	@Override
	public ClienteDto verDetalleCliente(Long idCliente) {
		Optional<ClienteEntity> clienteEntity = clienteRepository.findById(idCliente);
		ClienteDto clienteReturn =null;
		if(null!=clienteEntity && clienteEntity.isPresent()) {
			clienteReturn=clienteMapper.clientEntityToClienteDto(clienteEntity.get());
		}
		return clienteReturn;
	}

}
