package com.co.pragma.crecimiento.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.mapper.TipoIdentificacionMapper;
import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;
import com.co.pragma.crecimiento.repository.TipoIdentificacionRepository;
import com.co.pragma.crecimiento.service.TipoIdentificacionService;

@Service
public class TipoIdentificacionServiceImpl implements TipoIdentificacionService{

	@Autowired
	private TipoIdentificacionRepository tipoIdentificacionRepository;
	
	@Autowired
	private TipoIdentificacionMapper tipoIdentificacionMapper;
	
	@Override
	public TipoIdentificacionDto buscarPorId(Long id) {
		TipoIdentificacionEntity tipoEntity = tipoIdentificacionRepository.findById(id).orElse(null);
		return tipoIdentificacionMapper.tipoIdentificacionEntityToTipoIdentificacionDto(tipoEntity);
	}

}
