package com.co.pragma.crecimiento.service;

import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;

public interface TipoIdentificacionService {

	public TipoIdentificacionDto buscarPorId(Long id);
}
