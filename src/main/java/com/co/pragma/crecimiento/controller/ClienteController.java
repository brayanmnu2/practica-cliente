package com.co.pragma.crecimiento.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.service.ClienteService;
import com.co.pragma.crecimiento.util.UtilPractica;

@RestController
@RequestMapping(value="cliente")
public class ClienteController {

	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping(value="listarClientes")
	public ResponseEntity<List<ClienteDto>> listarClientes() {
		List<ClienteDto> listaClientes = clienteService.listarClientes();
		if( null==listaClientes || listaClientes.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(listaClientes);
	}
	
	@PostMapping(value="registrar")
	public ResponseEntity<ClienteDto> registrarCliente(@Valid @RequestBody ClienteDto clienteDto/*, BindingResult result*/){
//		UtilPractica util = new UtilPractica();
		/*if(result.hasErrors()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, util.formatMessage(result));
		}*/
		
		ClienteDto clienteReturn = clienteService.registrarCliente(clienteDto);
		if(null==clienteReturn) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteReturn);
	}
	
	
	@PutMapping(value="actualizar")
	public ResponseEntity<ClienteDto> actualizarCliente(@Valid @RequestBody ClienteDto clienteDto){
		if(null!=clienteDto.getId()) {
			ClienteDto clienteReturn = clienteService.actualizarCliente(clienteDto);
			if(null!=clienteReturn) {
				return ResponseEntity.ok(clienteReturn);
			}
		}
		return ResponseEntity.badRequest().build();
		
	}
	
	@DeleteMapping(value="eliminar/{id}")
	public ResponseEntity<ClienteDto> eliminarCliente(@PathVariable("id") Long id){
		boolean isEliminado = clienteService.eliminarClientePorId(id);
		if(!isEliminado) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(null);
	}
	
	@GetMapping(value="buscarPorTipoNroIdentificacion/{idTipo}/{nro}")
	public ResponseEntity<ClienteDto> buscarPorTipoNroIdentificacion(@PathVariable("idTipo") Long idTipo, @PathVariable("nro") String nro){
		ClienteDto clienteNro = clienteService.buscarPorTipoNroIdentificacion(idTipo, nro);
		if(null==clienteNro) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(clienteNro);
	}
	
	@GetMapping(value="buscarPorMayorIgualEdad/{edad}")
	public ResponseEntity<List<ClienteDto>> buscarPorMayorIgualEdad(@PathVariable("edad") int edad){
		List<ClienteDto> listaClientes = clienteService.buscarPorMayorIgualEdad(edad);
		if(null==listaClientes || listaClientes.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(listaClientes);
	}
	
	@GetMapping(value="verDetalleCliente/{idCliente}")
	public ResponseEntity<ClienteDto> verDetalleCliente(@PathVariable("idCliente") Long idCliente){
		ClienteDto clienteReturn = clienteService.verDetalleCliente(idCliente);
		if(null==clienteReturn) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(clienteReturn);
	}
	
	
}
