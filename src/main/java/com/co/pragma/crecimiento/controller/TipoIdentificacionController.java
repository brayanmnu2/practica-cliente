package com.co.pragma.crecimiento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.service.TipoIdentificacionService;

@RestController
@RequestMapping(value="tipo-identificacion")
public class TipoIdentificacionController {

	@Autowired
	private TipoIdentificacionService tipoIdentificacionService;
	
	@GetMapping(value="verDetalle/{id}")
	public ResponseEntity<TipoIdentificacionDto> verDetalleIdentificacion(@PathVariable("id") Long id){
		TipoIdentificacionDto identificacionReturn = tipoIdentificacionService.buscarPorId(id);
		if(null==identificacionReturn) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(identificacionReturn);
	}
}
