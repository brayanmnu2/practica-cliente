package com.co.pragma.crecimiento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class PracticaClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaClienteApplication.class, args);
	}

}
