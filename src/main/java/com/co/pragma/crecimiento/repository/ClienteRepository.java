package com.co.pragma.crecimiento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.co.pragma.crecimiento.model.ClienteEntity;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Long>{

	@Query("from ClienteEntity c where c.tipoIdentificacion.id=:idTipo and c.nroIdentificacion=:nroIdentificacion")
	ClienteEntity buscarPorTipoNroIdentificacion(@Param("idTipo") Long idTipo, @Param("nroIdentificacion")String nroIdentificacion);
	
	@Query("from ClienteEntity c where c.edad>=:edadFiltro")
	List<ClienteEntity> buscarPorMayorIgualEdad(@Param("edadFiltro") int edadFiltro);
	
}
