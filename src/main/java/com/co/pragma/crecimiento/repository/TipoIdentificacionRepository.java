package com.co.pragma.crecimiento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;

public interface TipoIdentificacionRepository extends JpaRepository<TipoIdentificacionEntity, Long>{

}
