package com.co.pragma.crecimiento;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.co.pragma.crecimiento.model.ClienteEntity;
import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;
import com.co.pragma.crecimiento.repository.TipoIdentificacionRepository;
import com.co.pragma.crecimiento.util.DatosTest;

@DataJpaTest
public class TipoIdentificacionRepositoryTest {

	@Autowired
	TipoIdentificacionRepository tipoIdentificacionRepository;
	
	@Test
	void testSave() {
		
		tipoIdentificacionRepository.save(DatosTest.TIPO_IDENTIFICACION_ENTITY_PASAPORTE);
		TipoIdentificacionEntity tipoReturn =  tipoIdentificacionRepository.findById(DatosTest.DOS_LONG).orElse(null);
		
		assertNotNull(tipoReturn);
		assertEquals(DatosTest.PASAPORTE_DESC,tipoReturn.getDescripcion());
	}
	
	@Test
	void testDelete() {
		TipoIdentificacionEntity tipoEliminar =  tipoIdentificacionRepository.findById(DatosTest.UNO_LONG).orElse(null);
		tipoIdentificacionRepository.delete(tipoEliminar);
		TipoIdentificacionEntity tipoConsulta = tipoIdentificacionRepository.findById(DatosTest.UNO_LONG).orElse(null);
		
		assertNull(tipoConsulta);
	}
}
