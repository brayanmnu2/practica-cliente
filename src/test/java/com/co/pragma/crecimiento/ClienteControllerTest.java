package com.co.pragma.crecimiento;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.co.pragma.crecimiento.controller.ClienteController;
import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.service.ClienteService;
import com.co.pragma.crecimiento.util.DatosTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

	ObjectMapper objectMapper;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ClienteService clienteService;

	@Spy
	ClienteMapper clienteMapper = Mappers.getMapper(ClienteMapper.class);

	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
	}

	@Test
	void testListarClientesExiste() throws Exception {

		List<ClienteDto> listClienteDto = clienteMapper.listClienteEntityToListClienteDto(DatosTest.LIST_CLIENTE_19);
		when(clienteService.listarClientes()).thenReturn(listClienteDto);
		mvc.perform(get("/cliente/listarClientes").contentType(MediaType.APPLICATION_JSON))
				// Then
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].id").value("1"))
				.andExpect(jsonPath("$[1].id").value("2"))
				.andExpect(jsonPath("$[2].id").value("3"))
				.andExpect(jsonPath("$[0].nombres").value("Brayan Michel"))
				.andExpect(jsonPath("$[1].nombres").value("Pedro"))
				.andExpect(jsonPath("$[2].nombres").value("Pablo"))
				.andExpect(jsonPath("$[0].apellidos").value("Neyra Uriarte"))
				.andExpect(jsonPath("$[1].apellidos").value("Armas"))
				.andExpect(jsonPath("$[2].apellidos").value("Nazario"))
				.andExpect(jsonPath("$", hasSize(3)))
				//comparacion mas robusta
				.andExpect(content().json(objectMapper.writeValueAsString(listClienteDto)));
		verify(clienteService).listarClientes();
	}

	@Test
	void testListarClientesNoExiste() throws Exception {

		when(clienteService.listarClientes()).thenReturn(null);
		mvc.perform(get("/cliente/listarClientes").contentType(MediaType.APPLICATION_JSON))
				// Then
				.andExpect(status().isNoContent());
		verify(clienteService).listarClientes();
	}
	
	@Test
	void testRegistrarClienteExitoso() throws JsonProcessingException, Exception {
		ClienteDto clientDtoMapper = clienteMapper.clientEntityToClienteDto(DatosTest.CLIENTE_ENTITY_001);
		
		when(clienteService.registrarCliente(any())).then(invocation ->{
            ClienteDto c = invocation.getArgument(0);
            c.setId(1L);
            return c;
        });
		
		mvc.perform(post("/cliente/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_SIN_ID_001)))
		.andExpect(status().isCreated())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(clientDtoMapper)));
		
		verify(clienteService).registrarCliente(any());
	}
	
	@Test
	void testRegistrarClienteNoExitoso() throws JsonProcessingException, Exception {
		
		when(clienteService.registrarCliente(any())).thenReturn(null);
		
		mvc.perform(post("/cliente/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_SIN_ID_001)))
		.andExpect(status().isBadRequest());
		
		verify(clienteService).registrarCliente(any());
	}
	
	@Test
	void testRegistrarClienteNoValid() throws JsonProcessingException, Exception {
		
		when(clienteService.registrarCliente(any())).thenReturn(null);
		
		mvc.perform(post("/cliente/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_SIN_ID_SIN_VALID_001)))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	void testActualizarClienteSinId() throws JsonProcessingException, Exception {
		when(clienteService.actualizarCliente(any())).thenReturn(null);
		
		mvc.perform(put("/cliente/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_SIN_ID_001)))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	void testActualizarClienteSinValid() throws JsonProcessingException, Exception {
		when(clienteService.actualizarCliente(DatosTest.CLIENTE_DTO_001)).thenReturn(DatosTest.CLIENTE_DTO_001);
		
		mvc.perform(put("/cliente/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_001_SIN_VALID)))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	void testActualizarClienteExitoso() throws JsonProcessingException, Exception {
		when(clienteService.actualizarCliente(any())).then(invocation ->{
            ClienteDto c = invocation.getArgument(0);
            return c;
        });
		
		mvc.perform(put("/cliente/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_001)))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.CLIENTE_DTO_001)));
		
		verify(clienteService).actualizarCliente(any());
	}
	
	@Test
	void testEliminarClienteExitoso() throws Exception {
		when(clienteService.eliminarClientePorId(anyLong())).thenReturn(true);
		
		mvc.perform(delete("/cliente/eliminar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
		
		verify(clienteService).eliminarClientePorId(anyLong());
	}
	
	@Test
	void testEliminarClienteNoExitoso() throws Exception {
		when(clienteService.eliminarClientePorId(anyLong())).thenReturn(false);
		
		mvc.perform(delete("/cliente/eliminar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
		
		verify(clienteService).eliminarClientePorId(anyLong());
	}
	
	@Test
	void testBuscarPorTipoNroIdentificacionExitoso() throws JsonProcessingException, Exception {
		ClienteDto clienteDto = clienteMapper.clientEntityToClienteDto(DatosTest.CLIENTE_ENTITY_001);
		when(clienteService.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_001))
		.thenReturn(clienteDto);
		
		mvc.perform(get("/cliente/buscarPorTipoNroIdentificacion/1/75153630").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").value("1"))
		.andExpect(jsonPath("$.nombres").value("Brayan Michel"))
		.andExpect(jsonPath("$.apellidos").value("Neyra Uriarte"))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(clienteDto)));
		
		verify(clienteService).buscarPorTipoNroIdentificacion(anyLong(), anyString());
	}
	
	@Test
	void testBuscarPorTipoNroIdentificacionNoExitoso() throws Exception {
		when(clienteService.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_001))
		.thenReturn(null);
		
		mvc.perform(get("/cliente/buscarPorTipoNroIdentificacion/1/75153630").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isNoContent());
		
		verify(clienteService).buscarPorTipoNroIdentificacion(anyLong(), anyString());
	}
	
	@Test
	void testBuscarPorMayorIgualEdadExitoso() throws JsonProcessingException, Exception {
		List<ClienteDto> listClienteDto = clienteMapper.listClienteEntityToListClienteDto(DatosTest.LIST_CLIENTE_22);
		when(clienteService.buscarPorMayorIgualEdad(DatosTest.EDAD_22)).thenReturn(listClienteDto);
		
		mvc.perform(get("/cliente/buscarPorMayorIgualEdad/22").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$[0].id").value("1"))
		.andExpect(jsonPath("$[1].id").value("3"))
		.andExpect(jsonPath("$[0].nombres").value("Brayan Michel"))
		.andExpect(jsonPath("$[1].nombres").value("Pablo"))
		.andExpect(jsonPath("$[0].apellidos").value("Neyra Uriarte"))
		.andExpect(jsonPath("$[1].apellidos").value("Nazario"))
		.andExpect(jsonPath("$", hasSize(2)))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(listClienteDto)));
		verify(clienteService).buscarPorMayorIgualEdad(anyInt());
	}
	
	@Test
	void testBuscarPorMayorIgualEdadNoExitoso() throws JsonProcessingException, Exception {
		when(clienteService.buscarPorMayorIgualEdad(DatosTest.EDAD_22)).thenReturn(null);
		
		mvc.perform(get("/cliente/buscarPorMayorIgualEdad/22").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isNoContent());
		verify(clienteService).buscarPorMayorIgualEdad(anyInt());
	}
	
	@Test
	void testVerDetalleClienteExitoso() throws JsonProcessingException, Exception {
		ClienteDto clienteDto = clienteMapper.clientEntityToClienteDto(DatosTest.CLIENTE_ENTITY_001);
		when(clienteService.verDetalleCliente(DatosTest.UNO_LONG)).thenReturn(clienteDto);
		
		mvc.perform(get("/cliente/verDetalleCliente/1").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").value("1"))
		.andExpect(jsonPath("$.nombres").value("Brayan Michel"))
		.andExpect(jsonPath("$.apellidos").value("Neyra Uriarte"))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(clienteDto)));
		
		verify(clienteService).verDetalleCliente(anyLong());
	}
	
	
	@Test
	void testVerDetalleClienteNoExitoso() throws JsonProcessingException, Exception {
		when(clienteService.verDetalleCliente(DatosTest.UNO_LONG)).thenReturn(null);
		
		mvc.perform(get("/cliente/verDetalleCliente/1").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isNoContent());
		
		verify(clienteService).verDetalleCliente(anyLong());
	}
}
