package com.co.pragma.crecimiento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.model.ClienteEntity;
import com.co.pragma.crecimiento.repository.ClienteRepository;
import com.co.pragma.crecimiento.service.impl.ClienteServiceImpl;
import com.co.pragma.crecimiento.util.DatosTest;

@SpringBootTest
public class ClienteServiceTest {
	@Mock
	ClienteRepository clienteRepository;

	@Spy
	ClienteMapper clienteMapper = Mappers.getMapper(ClienteMapper.class);

	@InjectMocks
	ClienteServiceImpl clienteService;

	@Test
	void testListarClientesConDatos() {
		when(clienteRepository.findAll()).thenReturn(DatosTest.LIST_CLIENTE_19);
		
		List<ClienteDto> listClienteReturn = clienteService.listarClientes();
		
		assertNotNull(listClienteReturn);
		assertThat(listClienteReturn).isNotEmpty();
		assertTrue(listClienteReturn.stream()
				.anyMatch(cliente->"75153632".equals(cliente.getNroIdentificacion())));
	}
	
	@Test
	void testListarClientesSinDatos() {
		when(clienteRepository.findAll()).thenReturn(null);
		
		List<ClienteDto> listClienteReturn = clienteService.listarClientes();
		
		assertNull(listClienteReturn);
	}
	
	@Test
	void testRegistrarClienteExiste() {
		when(clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_004)).thenReturn(DatosTest.CLIENTE_ENTITY_004);
		ClienteDto clientReturn = clienteService.registrarCliente(DatosTest.CLIENTE_DTO_004);
		
		assertNull(clientReturn);
	}
	
	@Test
	void testRegistrarClienteNuevo() {
		when(clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_004)).thenReturn(null);
		when(clienteRepository.saveAndFlush(DatosTest.CLIENTE_ENTITY_SIN_ID_004)).thenReturn(DatosTest.CLIENTE_ENTITY_004);

		ClienteDto clientReturn = clienteService.registrarCliente(DatosTest.CLIENTE_DTO_004);
		
		assertNotNull(clientReturn);
		assertEquals("Juanito", clientReturn.getNombres());
	}
	
	@Test
	void  testActualizarClienteExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_001);
		when(clienteRepository.saveAndFlush(DatosTest.CLIENTE_ENTITY_ACTUALIZAR_001)).thenReturn(DatosTest.CLIENTE_ENTITY_ACTUALIZAR_001);
		
		ClienteDto clientReturn = clienteService.actualizarCliente(DatosTest.CLIENTE_DTO_001);
		
		assertNotNull(clientReturn);
		assertEquals("Brayitan Michel", clientReturn.getNombres());
	}
	
	@Test
	void  testActualizarClienteNoExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_EMPTY);
		
		ClienteDto clientReturn = clienteService.actualizarCliente(DatosTest.CLIENTE_DTO_001);
		
		assertNull(clientReturn);
	}
	
	@Test
	void testEliminarClientePorIdExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_001);
		// ACT (Actuar)
		boolean isEliminado = clienteService.eliminarClientePorId(DatosTest.UNO_LONG);

		// ASSERT (Afirmar)
		assertTrue(isEliminado);
		verify(clienteRepository).findById(anyLong());
	}

	
	@Test
	void testEliminarClientePorIdNoExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_EMPTY);
		// ACT (Actuar)
		boolean isEliminado = clienteService.eliminarClientePorId(DatosTest.UNO_LONG);

		// ASSERT (Afirmar)
		assertFalse(isEliminado);
	}
	
	
	@Test
	void testBuscarPorTipoNroIdentificacion() {
		// ARRANGE (Preparar)
		when(clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_001))
				.thenReturn(DatosTest.CLIENTE_ENTITY_001);
		when(clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.DOS_LONG, DatosTest.DNI_002))
				.thenReturn(DatosTest.CLIENTE_ENTITY_002);
		when(clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.TRES_LONG, DatosTest.DNI_003))
				.thenReturn(DatosTest.CLIENTE_ENTITY_003);
		// ACT (Actuar)
		ClienteDto clienteReturn = clienteService.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_001);

		// ASSERT (Afirmar)
		assertNotNull(clienteReturn);
		assertEquals("Brayan Michel", clienteReturn.getNombres());
		assertEquals("Neyra Uriarte", clienteReturn.getApellidos());
	}

	@Test
	void testBuscarPorMayorIgualEdad() {
		when(clienteRepository.buscarPorMayorIgualEdad(DatosTest.EDAD_19)).thenReturn(DatosTest.LIST_CLIENTE_19);
		// ACT (Actuar)
		List<ClienteDto> listClienteReturn = clienteService.buscarPorMayorIgualEdad(DatosTest.EDAD_19);

		// ASSERT (Afirmar)
		assertNotNull(listClienteReturn);
		assertThat(listClienteReturn).isNotEmpty();
		assertTrue(listClienteReturn.stream()
				.anyMatch(cliente->"Pedro".equals(cliente.getNombres())));
	}
	
	@Test 
	void testVerDetalleClienteExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_001);
		
		ClienteDto clienteReturn = clienteService.verDetalleCliente(DatosTest.UNO_LONG);
		
		assertNotNull(clienteReturn);
		assertEquals("Neyra Uriarte", clienteReturn.getApellidos());
	}
	
	
	@Test 
	void testVerDetalleClienteNoExiste() {
		when(clienteRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_ENTITY_OPTIONAL_EMPTY);
		
		ClienteDto clienteReturn = clienteService.verDetalleCliente(DatosTest.UNO_LONG);
		
		assertNull(clienteReturn);
	}
	
}
