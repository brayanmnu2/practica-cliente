package com.co.pragma.crecimiento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.co.pragma.crecimiento.model.ClienteEntity;
import com.co.pragma.crecimiento.model.TipoIdentificacionEntity;
import com.co.pragma.crecimiento.repository.ClienteRepository;
import com.co.pragma.crecimiento.util.DatosTest;

@DataJpaTest
public class ClienteRepositoryTest {

	@Autowired
	ClienteRepository clienteRepository;
	
	@Test
	void testBuscarPorTipoNroIdentificacion() {
		ClienteEntity cliente = clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_001);
		assertNotNull(cliente);
		assertEquals("Brayan Michel", cliente.getNombres());
		assertEquals("Neyra Uriarte", cliente.getApellidos());
		assertEquals("Trujillo", cliente.getCiudadNacimiento());
	}
	
	@Test
	void testBuscarPorMayorIgualEdad() {
		List<ClienteEntity> clienteList = clienteRepository.buscarPorMayorIgualEdad(DatosTest.EDAD_22);
		assertNotNull(clienteList);
		assertThat(clienteList).isNotEmpty();
		assertTrue(clienteList.stream()
				.anyMatch(cliente->DatosTest.DNI_003.equals(cliente.getNroIdentificacion())));
	}
	
	@Test
	void testSave() {
		
		clienteRepository.save(DatosTest.CLIENTE_ENTITY_004);
		ClienteEntity clienteReturn =  clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_004);
		
		assertNotNull(clienteReturn);
		assertEquals(DatosTest.DNI_004,clienteReturn.getNroIdentificacion());
	}
	
	@Test
	void testDelete() {
		ClienteEntity clienteEliminar =  clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_003);
		clienteRepository.delete(clienteEliminar);
		ClienteEntity clienteConsulta = clienteRepository.buscarPorTipoNroIdentificacion(DatosTest.UNO_LONG, DatosTest.DNI_003);
		
		assertNull(clienteConsulta);

	}
}
