package com.co.pragma.crecimiento;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.co.pragma.crecimiento.controller.TipoIdentificacionController;
import com.co.pragma.crecimiento.mapper.TipoIdentificacionMapper;
import com.co.pragma.crecimiento.service.TipoIdentificacionService;
import com.co.pragma.crecimiento.util.DatosTest;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TipoIdentificacionController.class)
public class TipoIdentificacionControllerTest {

	ObjectMapper objectMapper;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private TipoIdentificacionService identificacionService;

	@Spy
	TipoIdentificacionMapper tipoMapper = Mappers.getMapper(TipoIdentificacionMapper.class);

	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
	}
	
	@Test
	void testVerDetalleIdentificacionNoExitoso() throws Exception {
		when(identificacionService.buscarPorId(DatosTest.UNO_LONG)).thenReturn(null);
		
		mvc.perform(get("/tipo-identificacion/verDetalle/1").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isNoContent());
		
		verify(identificacionService).buscarPorId(anyLong());
	}
	
	
	@Test
	void testVerDetalleIdentificacionExitoso() throws Exception {
		when(identificacionService.buscarPorId(DatosTest.UNO_LONG)).thenReturn(DatosTest.TIPO_IDENTIFICACION_DTO_DNI);
		
		
		mvc.perform(get("/tipo-identificacion/verDetalle/1").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").value("1"))
		.andExpect(jsonPath("$.descripcion").value("DNI"))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.TIPO_IDENTIFICACION_DTO_DNI)));
		
		
		verify(identificacionService).buscarPorId(anyLong());
	}
	
	
}
