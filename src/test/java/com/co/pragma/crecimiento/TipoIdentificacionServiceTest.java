package com.co.pragma.crecimiento;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.mapper.TipoIdentificacionMapper;
import com.co.pragma.crecimiento.repository.TipoIdentificacionRepository;
import com.co.pragma.crecimiento.service.impl.ClienteServiceImpl;
import com.co.pragma.crecimiento.service.impl.TipoIdentificacionServiceImpl;
import com.co.pragma.crecimiento.util.DatosTest;

@SpringBootTest
public class TipoIdentificacionServiceTest {

	@Mock
	TipoIdentificacionRepository tipoRepository;

	@Spy
	TipoIdentificacionMapper tipoIdentificacionMapper = Mappers.getMapper(TipoIdentificacionMapper.class);

	@InjectMocks
	TipoIdentificacionServiceImpl tipoService;

	@Test
	void testBuscarPorIdExiste() {
		when(tipoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.TIPO_IDENTIFICACION_ENTITY_DNI_OPTIONAL);
		// ACT (Actuar)
		TipoIdentificacionDto tipoReturnDto = tipoService.buscarPorId(DatosTest.UNO_LONG);

		// ASSERT (Afirmar)
		assertNotNull(tipoReturnDto);
		assertEquals("DNI", tipoReturnDto.getDescripcion());
	}
	
	@Test
	void testBuscarPorIdNoExiste() {
		when(tipoRepository.findById(DatosTest.CUATRO_LONG)).thenReturn(DatosTest.TIPO_ENTITY_OPTIONAL_EMPTY);
		// ACT (Actuar)
		TipoIdentificacionDto tipoReturnDto = tipoService.buscarPorId(DatosTest.CUATRO_LONG);

		// ASSERT (Afirmar)
		assertNull(tipoReturnDto);
	}
}
